// IMPORT EXTERNAL LIBS
import { type TezosToolkit } from '@taquito/taquito'

// IMPORT SERVICES
import { getBalance } from '../../services/balance.service.js'

/**
 * Handles the process of retrieving the balance of the wallet.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<void>} A promise resolved once the balance retrieval process is complete.
 */
async function handleGetBalance (tezos: TezosToolkit): Promise<number> {
  try {
    return await getBalance(tezos)
  } catch (error) {
    throw new Error('An error occurred')
  }
}

export { handleGetBalance }
