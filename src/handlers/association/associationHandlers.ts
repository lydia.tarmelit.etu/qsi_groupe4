// IMPORT EXTERNAL LIBS
import { type TezosToolkit } from '@taquito/taquito'

// IMPORT TYPES
import { type Association } from '../../types/Association.js'

// IMPORT SERVICES
import { createAssociation, getAssociationDetails, getAssociations, joinAssociation } from '../../services/association.service.js'
import inquirer from 'inquirer'

/**
 * Handles the process of creating an association.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<void>} A promise resolved once the association creation process is complete.
 */
async function handleCreateAssociation (tezos: TezosToolkit): Promise<void> {
  const questions = [
    {
      type: 'input',
      name: 'name',
      message: 'Nom',
      validate: function (input: string) {
        const done = this.async()

        if (input.trim() === '') {
          done('Vous devez remplir ce champ')
        } else {
          done(null, true)
        }
      }
    },
    {
      type: 'input',
      name: 'description',
      message: 'Description',
      validate: function (input: string) {
        const done = this.async()

        if (input.trim() === '') {
          done('Vous devez remplir ce champ')
        } else {
          done(null, true)
        }
      }
    }
  ]

  let association: Association

  await inquirer.prompt(questions).then(async (answers: { name: string, description: string }) => {
    association = {
      name: answers.name,
      description: answers.description
    }
  })

  try {
    await createAssociation(association, tezos)
  } catch (error) {
    const errorMessage = error.lastError?.with?.string ? error.lastError.with.string : 'Unknown error occurred'
    throw new Error(`${errorMessage}`)
  }
}

/**
 * Handles the process of joining an association.
 * @param {string[]} associations - A list of association name.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<void>} A promise resolved once the joining process is complete.
 */
async function handleJoinAssociation (associations: string[], tezos: TezosToolkit): Promise<void> {
  const questions = [
    {
      type: 'list',
      name: 'choice',
      message: 'Quelle association voulez-vous rejoindre ?',
      choices: associations
    }
  ]

  let associationName: string

  await inquirer.prompt(questions).then(async (answers: { choice: string }) => {
    associationName = answers.choice
  })

  try {
    await joinAssociation(associationName, tezos)
  } catch (error) {
    const errorMessage = error.lastError?.with?.string ? error.lastError.with.string : 'Unknown error occurred'
    throw new Error(`${errorMessage}`)
  }
}

/**
 * Handles the process of listing associations.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<Association[]>} A promise with a list of string of association name.
 */
async function handleGetAssociations (tezos: TezosToolkit): Promise<Association[]> {
  try {
    return await getAssociations(tezos)
  } catch (error) {
    const errorMessage = error.message ? error.message : 'Unknown error occurred'
    throw new Error(`${errorMessage}`)
  }
}

/**
 * Handles the process of getting association details.
 * @param {string[]} associations - A list of association name.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<Association>} A promise resolved with association details.
 */
async function handleGetAssociationDetails (associations: string[], tezos: TezosToolkit): Promise<Association> {
  const questions = [
    {
      type: 'list',
      name: 'choice',
      message: 'De quelle association voulez-vous voir les détails ?',
      choices: associations
    }
  ]

  let associationName: string

  await inquirer.prompt(questions).then(async (answers: { choice: string }) => {
    associationName = answers.choice
  })

  try {
    return await getAssociationDetails(associationName, tezos)
  } catch (error) {
    const errorMessage = error.message ? error.message : 'Unknown error occurred'
    throw new Error(`${errorMessage}`)
  }
}

export { handleCreateAssociation, handleJoinAssociation, handleGetAssociations, handleGetAssociationDetails }
