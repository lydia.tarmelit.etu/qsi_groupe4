# QSI_Groupe4

## Prerequisites

Before you begin, make sure you have Node.js version 20 installed. We recommend using [Node Version Manager (NVM)](https://github.com/nvm-sh/nvm) for easy Node.js version management.

```bash
# Install NVM (Node Version Manager)
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

# Reload the shell configuration
source ~/.bashrc   # or source ~/.zshrc if you're using Zsh

# Install Node.js 20
nvm install 20

# Set Node.js 20 as the default version
nvm use 20
```

## Installation

1. Clone this repository:
   ```bash
   git clone https://gitlab.univ-lille.fr/lydia.tarmelit.etu/qsi_groupe4.git
   ```

2. Navigate to the project directory:
   ```bash
   cd qsi_groupe4
   ```

3. Install dependencies:
   ```bash
   npm install
   ```

## Available Scripts

In the project directory, you can run:

### `npm start main <filePath>`

To start the application with a JSON file as a parameter, use the following command:

```
npm start main <filePath>
```

Replace `<filePath>` with the path to your JSON file.

Your json should contain the following content:

```json
{
    "privateKey": "yourPrivateKey"
}
```

### `npm test`

To run tests, use the following command:

```
npm test
```

### `npm run lint`

To perform static code analysis with ESLint, use:

```
npm run lint
```

### `npm run lint:fix`

To automatically fix issues detected by ESLint, use:

```
npm run lint:fix
```
