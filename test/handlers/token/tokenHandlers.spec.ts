import { handleCreateToken, handleBurnToken } from '../../../src/handlers/token/tokenHandlers'
import { type TezosToolkit } from '@taquito/taquito'
import { vi, describe, it, expect, beforeEach } from 'vitest'

const { promptSpy, createFATokenSpy, burnTokenSpy } =
  vi.hoisted(() => {
    return {
      createFATokenSpy: vi.fn().mockResolvedValue({}),
      burnTokenSpy: vi.fn().mockResolvedValue({}),
      promptSpy: vi.fn().mockResolvedValue({})
    }
  })

vi.mock('inquirer', async (importOriginal) => {
  const actual = await importOriginal()

  if (typeof actual === 'object' && actual !== null) {
    return {
      ...actual,
      default: {
        prompt: promptSpy
      }
    }
  } else {
    return actual
  }
})

vi.mock('../../../src/services/token.service', () => ({
  createFAToken: createFATokenSpy,
  burnToken: burnTokenSpy
}))

const mockedTezosToolkit = {} as unknown as TezosToolkit

describe('tokenHandlers', () => {
  beforeEach(() => {
    vi.clearAllMocks()
  })

  describe('handleCreateToken', () => {
    describe('when createFAToken is called with success', () => {
      it('should create a token with provided tokens', async () => {
        const tokens = 5

        promptSpy.mockResolvedValueOnce({ tokens })

        await handleCreateToken(mockedTezosToolkit)

        expect(createFATokenSpy).toBeCalledWith(tokens, mockedTezosToolkit)
      })
    })

    describe('when createFAToken is called with error', () => {
      it('should throw error with correct message', async () => {
        const error = 'Custom Error'

        createFATokenSpy.mockRejectedValueOnce(error)
        const nbTokenFungible = 5

        promptSpy.mockResolvedValueOnce({ nbTokenFungible })

        await expect(handleCreateToken(mockedTezosToolkit)).rejects.toThrow('Custom Error')
      })
    })
  })

  describe('handleBurnToken', () => {
    describe('when burnToken is called with success', () => {
      it('should burn a token', async () => {
        await handleBurnToken(mockedTezosToolkit)

        expect(burnTokenSpy).toBeCalledWith(mockedTezosToolkit)
      })
    })

    describe('when burnToken is called with error', () => {
      it('should throw error with correct message', async () => {
        const error = {
          lastError: {
            with: {
              string: 'Custom Error'
            }
          }
        }
        burnTokenSpy.mockRejectedValueOnce(error)

        await expect(handleBurnToken(mockedTezosToolkit)).rejects.toThrow('Custom Error')
      })
    })
  })
})
