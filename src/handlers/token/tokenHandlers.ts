// IMPORT EXTERNAL LIBS
import { type TezosToolkit } from '@taquito/taquito'

// IMPORT SERVICE
import { burnToken, createFAToken, getTokenBalance } from '../../services/token.service.js'
import inquirer from 'inquirer'

/**
 * Handles the process of creating a fungible token.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<void>} A promise resolved once the token creation process is complete.
 */
async function handleCreateToken (tezos: TezosToolkit): Promise<void> {
  const questions = [
    {
      type: 'input',
      name: 'tokens',
      message: 'Nombre de token',
      validate: function (input: string) {
        const done = this.async()
        const parsedInput = parseFloat(input.trim())

        if (isNaN(parsedInput) || !Number.isInteger(parsedInput)) {
          done('Vous devez fournir un nombre entier')
        } else {
          done(null, true)
        }
      }
    }
  ]

  let tokens: number

  await inquirer.prompt(questions).then(async (answers: { tokens: string }) => {
    const nbToken = parseInt(answers.tokens)
    tokens = nbToken
  })

  try {
    await createFAToken(tokens, tezos)
  } catch (error) {
    throw new Error(`${error}`)
  }
}

/**
 * Handles the process of creating a proposal.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<void>} A promise resolved once the proposal creation process is complete.
 */
async function handleBurnToken (tezos: TezosToolkit): Promise<void> {
  try {
    await burnToken(tezos)
  } catch (error) {
    const errorMessage = error.lastError?.with?.string ? error.lastError.with.string : 'Unknown error occurred'
    throw new Error(`${errorMessage}`)
  }
}

/**
 * Handles the process of get token balance.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<void>} A promise resolved once the retrieve process is complete.
 */
async function handleGetTokenBalance (tezos: TezosToolkit): Promise<number> {
  try {
    return await getTokenBalance(tezos)
  } catch (error) {
    throw new Error(`${error}`)
  }
}

export { handleCreateToken, handleBurnToken, handleGetTokenBalance }
