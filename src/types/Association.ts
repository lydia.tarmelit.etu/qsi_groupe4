export interface Association {
  name: string
  description: string
  tokenReference?: string
  entranceFee?: number
}
