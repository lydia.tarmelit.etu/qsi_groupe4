import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { handleGetOpenProposals, handleCloseProposal } from '../../handlers/proposal/proposalHandlers.js'

/**
 * Closes a proposal
 * @param {TezosToolkit} tezos - The instance of the Tezos toolkit.
 * @returns {Promise<void>} A promise resolved once the proposal is closed.
 */
async function closeProposal (tezos: TezosToolkit): Promise<void> {
  await handleGetOpenProposals(tezos).then(async (response) => {
    const proposalsByTitle: string[] = response.map(proposal => proposal.title)

    if (response.length === 0) throw new Error("Aucune proposition n'est ouverte")

    await handleCloseProposal(proposalsByTitle, tezos).then(() => {
      console.log(chalk.bgGreenBright('\nVous avez clôturé la proposition !!'))
    }).catch((error) => {
      console.log(chalk.bgRed(`\n${error.message}`))
    })
  }).catch((error) => {
    console.log(chalk.bgRed(`\n${error.message}`))
  })
}

export { closeProposal }
