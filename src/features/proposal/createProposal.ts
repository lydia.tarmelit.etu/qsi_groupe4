import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { handleCreateProposal } from '../../handlers/proposal/proposalHandlers.js'

/**
 * Creates a proposal.
 * @param {TezosToolkit} tezos - The instance of the Tezos toolkit.
 * @returns {Promise<void>} A promise resolved once the proposal is created.
 */
async function createProposal (tezos: TezosToolkit): Promise<void> {
  await handleCreateProposal(tezos).then(() => {
    console.log(chalk.bgGreenBright('\nVous avez soumis une proposition !!'))
  }).catch((error) => {
    console.log(chalk.bgRed(`\n${error.message}`))
  })
}

export { createProposal }
