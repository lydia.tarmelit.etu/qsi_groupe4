import { handleGetBalance } from '../../../src/handlers/balance/balanceHandlers'
import { type TezosToolkit } from '@taquito/taquito'
import { vi, describe, it, expect, beforeEach } from 'vitest'

const { getBalanceSpy } =
  vi.hoisted(() => {
    return {
      getBalanceSpy: vi.fn().mockResolvedValue({}),
      promptSpy: vi.fn().mockResolvedValue({})
    }
  })

vi.mock('../../../src/services/balance.service', () => ({
  getBalance: getBalanceSpy
}))

const mockedTezosToolkit = {} as unknown as TezosToolkit

describe('balanceHandlers', () => {
  beforeEach(() => {
    vi.clearAllMocks()
  })

  describe('handleGetBalance', () => {
    describe('when getBalance is called with success', () => {
      it('should call getBalance', async () => {
        await handleGetBalance(mockedTezosToolkit)

        expect(getBalanceSpy).toBeCalledWith(mockedTezosToolkit)
      })
    })

    describe('when getBalance is called with error', () => {
      it('should throw error with correct message', async () => {
        getBalanceSpy.mockRejectedValueOnce({})

        await expect(handleGetBalance(mockedTezosToolkit)).rejects.toThrow('An error occurred')
      })
    })
  })
})
