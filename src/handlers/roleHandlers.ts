import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { showAssociations } from '../features/association/showAssociations.js'
import { showAssociationDetails } from '../features/association/showAssociationDetails.js'
import { showBalance } from '../features/balance/showBalance.js'
import { joinAssociation } from '../features/association/joinAssociation.js'
import { createAssociation } from '../features/association/createAssociation.js'
import { burnToken } from '../features/token/burnToken.js'
import { createToken } from '../features/token/createToken.js'
import { createProposal } from '../features/proposal/createProposal.js'
import { resolveProposal } from '../features/proposal/resolveProposal.js'
import { showProposals } from '../features/proposal/showProposals.js'
import { closeProposal } from '../features/proposal/closeProposal.js'
import { voteProposal } from '../features/proposal/voteProposal.js'
import { showTokenBalance } from '../features/token/showTokenBalance.js'

/**
   * Handles administrator actions based on the specified choice.
   * @param {string} choice - The administrator's choice.
   * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
   * @returns {Promise<void>} A promise resolved once the processing is complete.
   */
async function handleAdminChoice (choice: string, tezos: TezosToolkit): Promise<void> {
  switch (choice) {
    case 'Créer un token':
      await createToken(tezos)
      break
    case 'Brûler un token':
      await burnToken(tezos)
      break
    case 'Résoudre une proposition':
      await resolveProposal(tezos)
      break
    case 'Clôturer une proposition':
      await closeProposal(tezos)
      break
    case 'Voir les propositions':
      await showProposals(tezos)
      break
    case 'Voir ma balance de tokens':
      await showTokenBalance(tezos)
      break
    case 'Voir mon portefeuille':
      await showBalance(tezos)
      break
    default:
      console.log(chalk.bgRedBright('\nChoix invalide\n'))
      break
  }
}

/**
   * Handles adherent actions based on the specified choice.
   * @param {string} choice - The adherent's choice.
   * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
   * @returns {Promise<void>} A promise resolved once the processing is complete.
   */
async function handleAdherentChoice (choice: string, tezos: TezosToolkit): Promise<void> {
  switch (choice) {
    case 'Faire une proposition':
      await createProposal(tezos)
      break
    case 'Créer un token':
      await createToken(tezos)
      break
    case 'Voir les propositions':
      await showProposals(tezos)
      break
    case 'Voter pour une proposition':
      await voteProposal(tezos)
      break
    case 'Voir mon portefeuille':
      await showBalance(tezos)
      break
    case 'Brûler un token':
      await burnToken(tezos)
      break
    case 'Voir ma balance de tokens':
      await showTokenBalance(tezos)
      break
    default:
      console.log(chalk.bgRedBright('\nChoix invalide\n'))
      break
  }
}

/**
   * Handles connected actions based on the specified choice.
   * @param {string} choice - The connected's choice.
   * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
   * @returns {Promise<void>} A promise resolved once the processing is complete.
   */
async function handleConnectedChoice (choice: string, tezos: TezosToolkit): Promise<void> {
  switch (choice) {
    case 'Rejoindre une association':
      await joinAssociation(tezos)
      break
    case 'Créer un token':
      await createToken(tezos)
      break
    case 'Voir les associations':
      await showAssociations(tezos)
      break
    case "Voir les détails d'une association":
      await showAssociationDetails(tezos)
      break
    case 'Voir mon portefeuille':
      await showBalance(tezos)
      break
    case 'Créer une association':
      await createAssociation(tezos)
      break
    case 'Brûler un token':
      await burnToken(tezos)
      break
    case 'Voir ma balance de tokens':
      await showTokenBalance(tezos)
      break
    default:
      console.log(chalk.bgRedBright('\nChoix invalide\n'))
      break
  }
}

export { handleAdminChoice, handleAdherentChoice, handleConnectedChoice }
