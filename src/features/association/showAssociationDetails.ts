import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { handleGetAssociationDetails, handleGetAssociations } from '../../handlers/association/associationHandlers.js'

/**
 * Displays details of association.
 * @param {TezosToolkit} tezos - The instance of the Tezos toolkit.
 * @returns {Promise<void>} A promise resolved once the association details are displayed.
 */
async function showAssociationDetails (tezos: TezosToolkit): Promise<void> {
  await handleGetAssociations(tezos).then(async (response) => {
    const associationsByName = response.map(association => association.name)

    if (response.length === 0) throw new Error('Aucune association existante')

    await handleGetAssociationDetails(associationsByName, tezos).then((details) => {
      const associationDetails = details
      console.log(`${chalk.cyan('Nom:')} ${chalk.yellow(associationDetails.name)}`)
      console.log(`${chalk.cyan('Description:')} ${chalk.yellow(associationDetails.description)}`)
    }).catch((error) => {
      console.log(chalk.bgRed(`\n${error.message}`))
    })
  }).catch((error) => {
    console.log(chalk.bgRed(`\n${error.message}`))
  })
}

export { showAssociationDetails }
