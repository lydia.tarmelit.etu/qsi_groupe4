import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { handleGetProposals } from '../../handlers/proposal/proposalHandlers.js'

/**
 * Displays proposals.
 * @param {TezosToolkit} tezos - The instance of the Tezos toolkit.
 * @returns {Promise<void>} A promise resolved once the proposals are displayed.
 */
async function showProposals (tezos: TezosToolkit): Promise<void> {
  await handleGetProposals(tezos).then((response) => {
    const proposalsByTitle = response.map(proposal => proposal.title)

    if (response.length === 0) throw new Error('Aucune proposition existante')

    proposalsByTitle.forEach(title => { console.log(chalk.yellow(title)) })
  }).catch((error) => {
    console.log(chalk.bgRed(`\n${error.message}`))
  })
}

export { showProposals }
