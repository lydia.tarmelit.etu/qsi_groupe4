import { handleCloseProposal, handleCreateProposal, handleGetOpenProposals, handleGetProposals, handleGetSolvableProposals, handleResolveProposal } from '../../../src/handlers/proposal/proposalHandlers'
import { type TezosToolkit } from '@taquito/taquito'
import { vi, describe, it, expect, beforeEach } from 'vitest'

const { promptSpy, createProposalSpy, getProposalsSpy, closeProposalSpy, resolveProposalSpy } =
  vi.hoisted(() => {
    return {
      createProposalSpy: vi.fn().mockResolvedValue({}),
      getProposalsSpy: vi.fn().mockResolvedValue({}),
      closeProposalSpy: vi.fn().mockResolvedValue({}),
      resolveProposalSpy: vi.fn().mockResolvedValue({}),
      promptSpy: vi.fn().mockResolvedValue({})
    }
  })

vi.mock('inquirer', async (importOriginal) => {
  const actual = await importOriginal()

  if (typeof actual === 'object' && actual !== null) {
    return {
      ...actual,
      default: {
        prompt: promptSpy
      }
    }
  } else {
    return actual
  }
})

vi.mock('../../../src/services/proposal.service', () => ({
  createProposal: createProposalSpy,
  getProposals: getProposalsSpy,
  closeProposal: closeProposalSpy,
  resolveProposal: resolveProposalSpy
}))

const mockedTezosToolkit = {} as unknown as TezosToolkit

describe('proposalHandlers', () => {
  beforeEach(() => {
    vi.clearAllMocks()
  })

  describe('handleCreateProposal', () => {
    describe('when createProposal is called with success', () => {
      it('should create a proposal with provided title and description', async () => {
        const title = 'Proposal Title'
        const description = 'Proposal Description'

        promptSpy.mockResolvedValueOnce({ title, description })
        await handleCreateProposal(mockedTezosToolkit)

        expect(createProposalSpy).toBeCalledWith({ title: 'Proposal Title', description: 'Proposal Description' }, mockedTezosToolkit)
      })
    })

    describe('when createProposal is called with error', () => {
      it('should throw error with correct message', async () => {
        const error = {
          lastError: {
            with: {
              string: 'Custom Error'
            }
          }
        }
        createProposalSpy.mockRejectedValueOnce(error)
        const title = 'Proposal Title'
        const description = 'Proposal Description'

        promptSpy.mockResolvedValueOnce({ title, description })

        await expect(handleCreateProposal(mockedTezosToolkit)).rejects.toThrow('Custom Error')
      })
    })
  })

  describe('handleCloseProposal', () => {
    describe('when closeProposal is called with success', () => {
      it('should close proposal with provided title', async () => {
        const proposals = ['Proposal 1', 'Proposal 2']
        const proposalChoose = 'Proposal 1'

        promptSpy.mockResolvedValueOnce({ choice: proposalChoose })
        await handleCloseProposal(proposals, mockedTezosToolkit)

        expect(closeProposalSpy).toBeCalledWith(proposalChoose, mockedTezosToolkit)
      })
    })

    describe('when closeProposal is called with error', () => {
      it('should throw error with correct message', async () => {
        const error = {
          lastError: {
            with: {
              string: 'Custom Error'
            }
          }
        }
        closeProposalSpy.mockRejectedValueOnce(error)
        const proposals = ['Proposal 1', 'Proposal 2']
        const proposalChoose = 'Proposal 1'

        promptSpy.mockResolvedValueOnce({ choice: proposalChoose })

        await expect(handleCloseProposal(proposals, mockedTezosToolkit)).rejects.toThrow('Custom Error')
      })
    })
  })

  describe('handleResolveProposal', () => {
    describe('when resolveProposal is called with success', () => {
      it('should resolve proposal with provided title', async () => {
        const proposals = ['Proposal 1', 'Proposal 2']
        const proposalChoose = 'Proposal 1'

        promptSpy.mockResolvedValueOnce({ choice: proposalChoose })
        await handleResolveProposal(proposals, mockedTezosToolkit)

        expect(resolveProposalSpy).toBeCalledWith(proposalChoose, mockedTezosToolkit)
      })
    })

    describe('when resolveProposal is called with error', () => {
      it('should throw error with correct message', async () => {
        const error = {
          lastError: {
            with: {
              string: 'Custom Error'
            }
          }
        }
        resolveProposalSpy.mockRejectedValueOnce(error)
        const proposals = ['Proposal 1', 'Proposal 2']
        const proposalChoose = 'Proposal 1'

        promptSpy.mockResolvedValueOnce({ choice: proposalChoose })

        await expect(handleResolveProposal(proposals, mockedTezosToolkit)).rejects.toThrow('Custom Error')
      })
    })
  })

  describe('handleGetProposals', () => {
    describe('when getProposals is called with success', () => {
      it('should call getProposals and return proposal', async () => {
        getProposalsSpy.mockResolvedValueOnce([
          {
            title: 'Proposal 1',
            description: 'Proposal 1 description'
          },
          {
            title: 'Proposal 2',
            description: 'Proposal 2 description'
          }
        ])
        const proposals = await handleGetProposals(mockedTezosToolkit)

        expect(getProposalsSpy).toBeCalledWith(mockedTezosToolkit)
        expect(proposals).toStrictEqual(
          [
            {
              description: 'Proposal 1 description',
              title: 'Proposal 1'
            },
            {
              description: 'Proposal 2 description',
              title: 'Proposal 2'
            }
          ]
        )
      })
    })

    describe('when getProposals is called with error', () => {
      it('should throw error with correct message', async () => {
        const error = {
          message: 'Custom Error'
        }
        getProposalsSpy.mockRejectedValueOnce(error)

        await expect(handleGetProposals(mockedTezosToolkit)).rejects.toThrow('Custom Error')
      })
    })
  })

  describe('handleGetOpenProposals', () => {
    describe('when getProposals is called with success', () => {
      it('should call getProposals and return only open proposal', async () => {
        getProposalsSpy.mockResolvedValueOnce([
          {
            title: 'Proposal 1',
            description: 'Proposal 1 description',
            isOpen: true
          },
          {
            title: 'Proposal 2',
            description: 'Proposal 2 description',
            isOpen: false
          }
        ])
        const proposals = await handleGetOpenProposals(mockedTezosToolkit)

        expect(getProposalsSpy).toBeCalledWith(mockedTezosToolkit)
        expect(proposals).toStrictEqual(
          [
            {
              description: 'Proposal 1 description',
              isOpen: true,
              title: 'Proposal 1'
            }
          ]
        )
      })
    })

    describe('when getProposals is called with error', () => {
      it('should throw error with correct message', async () => {
        const error = {
          message: 'Custom Error'
        }
        getProposalsSpy.mockRejectedValueOnce(error)

        await expect(handleGetOpenProposals(mockedTezosToolkit)).rejects.toThrow('Custom Error')
      })
    })
  })

  describe('handleGetSolvableProposals', () => {
    describe('when getProposals is called with success', () => {
      it('should call getProposals and return only solvable proposal', async () => {
        getProposalsSpy.mockResolvedValueOnce([
          {
            title: 'Proposal 1',
            description: 'Proposal 1 description',
            approveRate: 49
          },
          {
            title: 'Proposal 2',
            description: 'Proposal 2 description',
            approveRate: 51
          }
        ])
        const proposals = await handleGetSolvableProposals(mockedTezosToolkit)

        expect(getProposalsSpy).toBeCalledWith(mockedTezosToolkit)
        expect(proposals).toStrictEqual(
          [
            {
              approveRate: 51,
              description: 'Proposal 2 description',
              title: 'Proposal 2'
            }
          ]
        )
      })
    })

    describe('when getProposals is called with error', () => {
      it('should throw error with correct message', async () => {
        const error = {
          message: 'Custom Error'
        }
        getProposalsSpy.mockRejectedValueOnce(error)

        await expect(handleGetSolvableProposals(mockedTezosToolkit)).rejects.toThrow('Custom Error')
      })
    })
  })
})
