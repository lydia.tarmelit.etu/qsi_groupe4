// IMPORT EXTERNAL LIBS
import { Command } from 'commander'
import chalk from 'chalk'
import fs from 'fs'
import { TezosToolkit } from '@taquito/taquito'
import { InMemorySigner } from '@taquito/signer'
import inquirer from 'inquirer'

// IMPORT UTILS
import { getAsciiArtText } from './src/utils/getAsciiArtText.js'
import { getRole } from './src/utils/getRole.js'

// IMPORT TYPES
import { type WalletData } from './src/types/WalletData.js'

// IMPORT HANDLERS
import { handleAdminChoice, handleAdherentChoice, handleConnectedChoice } from './src/handlers/roleHandlers.js'
import { type Role } from './src/types/Role.js'
import { hasToken } from './src/features/token/hasToken.js'

const tezos = new TezosToolkit('https://ghostnet.tezos.marigold.dev')
const program = new Command()

program.name('my-asso')
  .description('My Asso CLI')
  .version('1.0.0')

program.command('main')
  .argument('<filePath>', 'Chemin du fichier JSON du portefeuille')
  .action(async (file: string) => {
    // LOAD WALLET FROM JSON
    const walletData: WalletData = JSON.parse(fs.readFileSync(file, 'utf8'))

    // REGISTER THE PROVIDER
    const signer = new InMemorySigner(walletData.privateKey)
    tezos.setProvider({ signer })

    // DISPLAY HEADER
    await getAsciiArtText()

    // START MAIN APPLICATION
    while (true) {
      const role: Role = await getRole(tezos)

      console.log(`\nVous êtes connecté en tant que ${chalk.bgBlue(role)}\n`)

      const questions = [
        {
          type: 'list',
          name: 'choice',
          message: 'Que souhaitez-vous faire ?',
          choices: []
        }
      ]
      switch (role) {
        case 'ADMIN':
          questions[0].choices = ['Résoudre une proposition', 'Clôturer une proposition', 'Voir les propositions', 'Créer un token', 'Voir ma balance de tokens']

          if (await hasToken(tezos)) { questions[0].choices.push('Brûler un token') }

          await inquirer.prompt(questions).then(async (answers: { choice: string }) => {
            await handleAdminChoice(answers.choice, tezos)
          })
          break
        case 'ADHERENT':
          // TODO: CLOTURER ET RESOUDRE LES PROPOSITIONS DONT IL EST LE CREATEUR
          questions[0].choices = ['Faire une proposition', 'Voter pour une proposition', 'Voir les propositions', 'Créer un token', 'Voir ma balance de tokens']

          if (await hasToken(tezos)) { questions[0].choices.push('Brûler un token') }

          await inquirer.prompt(questions).then(async (answers: { choice: string }) => {
            await handleAdherentChoice(answers.choice, tezos)
          })
          break
        case 'CONNECTED':
          questions[0].choices = ['Rejoindre une association', 'Voir les associations', "Voir les détails d'une association", 'Créer un token', 'Voir ma balance de tokens']

          if (await hasToken(tezos)) { questions[0].choices.push('Brûler un token', 'Créer une association') }

          await inquirer.prompt(questions).then(async (answers: { choice: string }) => {
            await handleConnectedChoice(answers.choice, tezos)
          })
          break
      }
    }
  })

program.parse()
