import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { handleGetAssociations, handleJoinAssociation } from '../../handlers/association/associationHandlers.js'

/**
 * Joins an association.
 * @param {TezosToolkit} tezos - The instance of the Tezos toolkit.
 * @returns {Promise<void>} A promise resolved once the user joins the association.
 */
async function joinAssociation (tezos: TezosToolkit): Promise<void> {
  await handleGetAssociations(tezos).then(async (response) => {
    const associationsByName: string[] = response.map(association => association.name)

    await handleJoinAssociation(associationsByName, tezos).then(() => {
      console.log(chalk.bgGreenBright("\nVous avez rejoint l'association !!"))
    }).catch((error) => {
      console.log(chalk.bgRed(`\n${error.message}`))
    })
  }).catch((error) => {
    console.log(chalk.bgRed(`\n${error.message}`))
  })
}

export { joinAssociation }
