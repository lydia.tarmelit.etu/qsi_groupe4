import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { handleGetAssociations } from '../../handlers/association/associationHandlers.js'

/**
 * Displays associations.
 * @param {TezosToolkit} tezos - The instance of the Tezos toolkit.
 * @returns {Promise<void>} A promise resolved once the associations are displayed.
 */
async function showAssociations (tezos: TezosToolkit): Promise<void> {
  await handleGetAssociations(tezos).then((response) => {
    const associationsByName = response.map(association => association.name)

    if (response.length === 0) throw new Error('Aucune association existante')

    associationsByName.forEach(name => { console.log(chalk.yellow(name)) })
  }).catch((error) => {
    console.log(chalk.bgRed(`\n${error.message}`))
  })
}

export { showAssociations }
