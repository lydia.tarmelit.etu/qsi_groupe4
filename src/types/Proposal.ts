export interface Proposal {
  title: string
  description: string
  fct?: () => void
  participationRate?: number
  approveRate?: number
  isOpen?: boolean
}
