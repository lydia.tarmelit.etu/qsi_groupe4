import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { handleGetTokenBalance } from '../../handlers/token/tokenHandlers.js'

/**
 * Displays token balance.
 * @param {TezosToolkit} tezos - The instance of the Tezos toolkit.
 * @returns {Promise<void>} A promise resolved once the token balance is displayed.
 */
async function showTokenBalance (tezos: TezosToolkit): Promise<void> {
  await handleGetTokenBalance(tezos).then((response) => {
    console.log(`${chalk.cyan('Nombre de token:')} ${chalk.yellow(response)}`)
  }).catch((error) => {
    console.log(chalk.bgRed(`\n${error.message}`))
  })
}

export { showTokenBalance }
