import { type TezosToolkit } from '@taquito/taquito'

async function getBalance (tezos: TezosToolkit): Promise<number> {
  try {
    const balance = await tezos.tz.getBalance(await tezos.signer.publicKeyHash())
    return balance.toNumber() / 1000000
  } catch (error) {
    console.error('Erreur lors de la récupération du solde:', error)
    throw error
  }
}

export { getBalance }
