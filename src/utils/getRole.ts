import { type TezosToolkit } from '@taquito/taquito'
import { type Role } from '../types/Role'

export async function getRole (tezos: TezosToolkit): Promise<Role> {
  return 'CONNECTED'
}
