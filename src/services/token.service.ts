import { type Operation, type TezosToolkit } from '@taquito/taquito'

const address = 'KT1Jrg6G9ziKbpSvzPLhjM4pXWYJqxoZdhMZ'

async function createFAToken (nbTokens: number, tezos: TezosToolkit): Promise<void> {
  const contract = await tezos.contract.at(address)
  const op: Operation = await contract.methodsObject.createContract(nbTokens).send()

  await op.confirmation()
}

async function burnToken (tezos: TezosToolkit): Promise<void> {
  const contract = await tezos.contract.at(address)
  const op: Operation = await contract.methodsObject.burn().send()

  await op.confirmation()
}

async function getTokenBalance (tezos: TezosToolkit): Promise<number> {
  const contract = await tezos.contract.at(address)

  const executionContextParams = {
    viewCaller: contract.address
  }
  const balance = await contract.contractViews.get_balance(await tezos.signer.publicKeyHash()).executeView(executionContextParams)

  return balance.token.toNumber()
}

export { createFAToken, burnToken, getTokenBalance }
