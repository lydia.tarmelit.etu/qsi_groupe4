import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { handleGetOpenProposals, handleVoteProposal } from '../../handlers/proposal/proposalHandlers.js'

/**
 * Votes for a proposal.
 * @param {TezosToolkit} tezos - The instance of the Tezos toolkit.
 * @returns {Promise<void>} A promise resolved once the vote is submitted.
 */
async function voteProposal (tezos: TezosToolkit): Promise<void> {
  await handleGetOpenProposals(tezos).then(async (response) => {
    const proposalsByTitle: string[] = response.map(proposal => proposal.title)

    if (response.length === 0) throw new Error("Aucune proposition n'est ouverte")

    await handleVoteProposal(proposalsByTitle, tezos).then(() => {
      console.log(chalk.bgGreenBright('\nVous avez voté pour cette proposition !!'))
    }).catch((error) => {
      console.log(chalk.bgRed(`\n${error.message}`))
    })
  }).catch((error) => {
    console.log(chalk.bgRed(`\n${error.message}`))
  })
}

export { voteProposal }
