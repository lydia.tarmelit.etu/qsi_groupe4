import { type Operation, type TezosToolkit } from '@taquito/taquito'
import { type Proposal } from '../types/Proposal'
import { type Vote } from '../types/Vote'

// NEED UPDATE ADDRESS !! (SMART CONTRACT 2: Factory de DAO)
const address = 'KT1QZJzhSPQ89K4eC59tmQYCt44qph7wXoJu'

const mockProposals: Proposal[] = [
  {
    title: 'Proposal 1',
    description: 'Ceci est la proposition 1',
    isOpen: true,
    approveRate: 50
  },
  {
    title: 'Proposal 2',
    description: 'Ceci est la proposition 2'
  }
]

// NEED UPDATE ENTRYPOINT !!
async function createProposal (proposal: Proposal, tezos: TezosToolkit): Promise<void> {
  // const contract = await tezos.contract.at(address)
  // const op: Operation = await contract.methodsObject.createProposal(proposal).send()

  // await op.confirmation()
}

// NEED UPDATE ENTRYPOINT !!
async function voteForProposal (proposalName: string, vote: Vote, tezos: TezosToolkit): Promise<void> {
  const proposalVote = {
    proposalName,
    proposalVote: vote
  }

  // const contract = await tezos.contract.at(address)
  // const op: Operation = await contract.methodsObject.voteForProposal(proposalVote).send()

  // await op.confirmation()
}

// NEED UPDATE ENTRYPOINT !!
async function closeProposal (proposalName: string, tezos: TezosToolkit): Promise<void> {
  // const contract = await tezos.contract.at(address)
  // const op: Operation = await contract.methodsObject.closeProposal(proposalName).send()

  // await op.confirmation()
}

// NEED UPDATE ENTRYPOINT !!
async function resolveProposal (proposalName: string, tezos: TezosToolkit): Promise<void> {
  // const contract = await tezos.contract.at(address)
  // const op: Operation = await contract.methodsObject.resolveProposal(proposalName).send()

  // await op.confirmation()
}

// NEED UPDATE ENTRYPOINT !!
async function viewVoteInProgressProposal (proposalName: string, tezos: TezosToolkit): Promise<void> {
  // const contract = await tezos.contract.at(address)
  // const op: Operation = await contract.methodsObject.viewVoteInProgressProposal(proposalName).send()

  // await op.confirmation()
}

// NEED UPDATE ENTRYPOINT !!
async function getProposals (tezos: TezosToolkit): Promise<Proposal[]> {
  // const contract = await tezos.contract.at(address)
  // const op: Operation = await contract.methodsObject.resolveProposal(proposalName).send()

  // await op.confirmation()

  // MOCK
  return mockProposals
}

export { createProposal, voteForProposal, viewVoteInProgressProposal, closeProposal, resolveProposal, getProposals }
