import { type TezosToolkit } from '@taquito/taquito'
import { handleAdminChoice, handleAdherentChoice, handleConnectedChoice } from '../../src/handlers/roleHandlers'
import { vi, describe, it, expect, beforeEach } from 'vitest'
import chalk from 'chalk'
import { createAssociation } from '../../src/features/association/createAssociation'
import { burnToken } from '../../src/features/token/burnToken'
import { createToken } from '../../src/features/token/createToken'
import { showBalance } from '../../src/features/balance/showBalance'
import { createProposal } from '../../src/features/proposal/createProposal'
import { joinAssociation } from '../../src/features/association/joinAssociation'
import { showAssociations } from '../../src/features/association/showAssociations'
import { resolveProposal } from '../../src/features/proposal/resolveProposal'
import { showProposals } from '../../src/features/proposal/showProposals'
import { closeProposal } from '../../src/features/proposal/closeProposal'
import { showTokenBalance } from '../../src/features/token/showTokenBalance'

vi.mock('../../src/features/association/createAssociation', () => ({
  createAssociation: vi.fn()
}))

vi.mock('../../src/features/token/burnToken', () => ({
  burnToken: vi.fn()
}))

vi.mock('../../src/features/proposal/createProposal', () => ({
  createProposal: vi.fn()
}))

vi.mock('../../src/features/token/createToken', () => ({
  createToken: vi.fn()
}))

vi.mock('../../src/features/association/joinAssociation', () => ({
  joinAssociation: vi.fn()
}))

vi.mock('../../src/features/association/showAssociationDetails', () => ({
  showAssociationDetails: vi.fn()
}))

vi.mock('../../src/features/association/showAssociations', () => ({
  showAssociations: vi.fn()
}))

vi.mock('../../src/features/balance/showBalance', () => ({
  showBalance: vi.fn()
}))

vi.mock('../../src/features/proposal/resolveProposal', () => ({
  resolveProposal: vi.fn()
}))

vi.mock('../../src/features/proposal/closeProposal', () => ({
  closeProposal: vi.fn()
}))

vi.mock('../../src/features/proposal/showProposals', () => ({
  showProposals: vi.fn()
}))

vi.mock('../../src/features/token/showTokenBalance', () => ({
  showTokenBalance: vi.fn()
}))

const mockedTezosToolkit = {} as unknown as TezosToolkit

describe('roleHandlers', () => {
  beforeEach(() => {
    vi.clearAllMocks()
  })

  describe('handleAdminChoice', () => {
    describe('when choice is "Créer un token"', () => {
      it('should call createToken', async () => {
        await handleAdminChoice('Créer un token', mockedTezosToolkit)

        expect(createToken).toBeCalled()
      })
    })

    describe('when choice is "Brûler un token"', () => {
      it('should call burnToken', async () => {
        await handleAdminChoice('Brûler un token', mockedTezosToolkit)

        expect(burnToken).toBeCalled()
      })
    })

    describe('when choice is "Résoudre une proposition"', () => {
      it('should call resolveProposal', async () => {
        await handleAdminChoice('Résoudre une proposition', mockedTezosToolkit)

        expect(resolveProposal).toBeCalled()
      })
    })

    describe('when choice is "Voir les propositions"', () => {
      it('should call showProposals', async () => {
        await handleAdminChoice('Voir les propositions', mockedTezosToolkit)

        expect(showProposals).toBeCalled()
      })
    })

    describe('when choice is "Clôturer une proposition"', () => {
      it('should call closeProposal', async () => {
        await handleAdminChoice('Clôturer une proposition', mockedTezosToolkit)

        expect(closeProposal).toBeCalled()
      })
    })

    describe('when choice is "Voir ma balance de tokens"', () => {
      it('should call showTokenBalance', async () => {
        await handleAdminChoice('Voir ma balance de tokens', mockedTezosToolkit)

        expect(showTokenBalance).toBeCalled()
      })
    })

    describe('when choice is "Voir mon portefeuille"', () => {
      it('should call showBalance', async () => {
        await handleAdminChoice('Voir mon portefeuille', mockedTezosToolkit)

        expect(showBalance).toBeCalled()
      })
    })

    describe('when choice is invalid', () => {
      it('should log "Choix invalide"', async () => {
        const consoleSpy = vi.spyOn(console, 'log').mockImplementation(() => {})

        await handleAdminChoice('invalid', mockedTezosToolkit)

        expect(consoleSpy).toHaveBeenCalledWith(chalk.bgRedBright('\nChoix invalide\n'))
      })
    })
  })

  describe('handleAdherentChoice', () => {
    describe('when choice is "Faire une proposition"', () => {
      it('should call createProposal', async () => {
        await handleAdherentChoice('Faire une proposition', mockedTezosToolkit)

        expect(createProposal).toBeCalled()
      })
    })

    describe('when choice is "Créer un token"', () => {
      it('should call createToken', async () => {
        await handleAdherentChoice('Créer un token', mockedTezosToolkit)

        expect(createToken).toBeCalled()
      })
    })

    describe('when choice is "Voir mon portefeuille"', () => {
      it('should call showBalance', async () => {
        await handleAdherentChoice('Voir mon portefeuille', mockedTezosToolkit)

        expect(showBalance).toBeCalled()
      })
    })

    describe('when choice is "Brûler un token"', () => {
      it('should call burnToken', async () => {
        await handleAdherentChoice('Brûler un token', mockedTezosToolkit)

        expect(burnToken).toBeCalled()
      })
    })

    describe('when choice is "Voir les propositions"', () => {
      it('should call showProposals', async () => {
        await handleAdherentChoice('Voir les propositions', mockedTezosToolkit)

        expect(showProposals).toBeCalled()
      })
    })

    describe('when choice is "Voir ma balance de tokens"', () => {
      it('should call showTokenBalance', async () => {
        await handleAdherentChoice('Voir ma balance de tokens', mockedTezosToolkit)

        expect(showTokenBalance).toBeCalled()
      })
    })

    describe('when choice is invalid', () => {
      it('should log "Choix invalide"', async () => {
        const consoleSpy = vi.spyOn(console, 'log').mockImplementation(() => {})
        await handleAdherentChoice('invalid', mockedTezosToolkit)

        expect(consoleSpy).toHaveBeenCalledWith(chalk.bgRedBright('\nChoix invalide\n'))
      })
    })
  })

  describe('handleConnectedChoice', () => {
    describe('when choice is "Rejoindre une association"', () => {
      it('should call joinAssociation', async () => {
        await handleConnectedChoice('Rejoindre une association', mockedTezosToolkit)

        expect(joinAssociation).toBeCalled()
      })
    })

    describe('when choice is "Créer un token"', () => {
      it('should call createToken', async () => {
        await handleConnectedChoice('Créer un token', mockedTezosToolkit)

        expect(createToken).toBeCalled()
      })
    })

    describe('when choice is "Voir les associations"', () => {
      it('should call showAssociations', async () => {
        await handleConnectedChoice('Voir les associations', mockedTezosToolkit)

        expect(showAssociations).toBeCalled()
      })
    })

    describe('when choice is "Voir mon portefeuille"', () => {
      it('should call showBalance', async () => {
        await handleConnectedChoice('Voir mon portefeuille', mockedTezosToolkit)

        expect(showBalance).toBeCalled()
      })
    })

    describe('when choice is "Créer une association"', () => {
      it('should call createAssociation', async () => {
        await handleConnectedChoice('Créer une association', mockedTezosToolkit)

        expect(createAssociation).toBeCalled()
      })
    })

    describe('when choice is "Brûler un token"', () => {
      it('should call burnToken', async () => {
        await handleConnectedChoice('Brûler un token', mockedTezosToolkit)

        expect(burnToken).toBeCalled()
      })
    })

    describe('when choice is "Voir ma balance de tokens"', () => {
      it('should call showTokenBalance', async () => {
        await handleConnectedChoice('Voir ma balance de tokens', mockedTezosToolkit)

        expect(showTokenBalance).toBeCalled()
      })
    })

    describe('when choice is invalid', () => {
      it('should log "Choix invalide"', async () => {
        const consoleSpy = vi.spyOn(console, 'log').mockImplementation(() => {})
        await handleConnectedChoice('invalid', mockedTezosToolkit)

        expect(consoleSpy).toHaveBeenCalledWith(chalk.bgRedBright('\nChoix invalide\n'))
      })
    })
  })
})
