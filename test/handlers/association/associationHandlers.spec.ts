import { handleCreateAssociation, handleJoinAssociation, handleGetAssociations, handleGetAssociationDetails } from '../../../src/handlers/association/associationHandlers'
import { type TezosToolkit } from '@taquito/taquito'
import { vi, describe, it, expect, beforeEach } from 'vitest'

const { createAssociationSpy, promptSpy, joinAssociationSpy, getAssociationsSpy, getAssociationDetailsSpy } =
  vi.hoisted(() => {
    return {
      createAssociationSpy: vi.fn().mockResolvedValue({}),
      joinAssociationSpy: vi.fn().mockResolvedValue({}),
      getAssociationsSpy: vi.fn().mockResolvedValue({}),
      getAssociationDetailsSpy: vi.fn().mockResolvedValue({}),
      promptSpy: vi.fn().mockResolvedValue({})
    }
  })

vi.mock('inquirer', async (importOriginal) => {
  const actual = await importOriginal()

  if (typeof actual === 'object' && actual !== null) {
    return {
      ...actual,
      default: {
        prompt: promptSpy
      }
    }
  } else {
    return actual
  }
})

vi.mock('../../../src/services/association.service', () => ({
  createAssociation: createAssociationSpy,
  joinAssociation: joinAssociationSpy,
  getAssociations: getAssociationsSpy,
  getAssociationDetails: getAssociationDetailsSpy
}))

const mockedTezosToolkit = {} as unknown as TezosToolkit

describe('associationHandlers', () => {
  beforeEach(() => {
    vi.clearAllMocks()
  })

  describe('handleCreateAssociation', () => {
    describe('when createAssociation is called with success', () => {
      it('should create an association with provided name and description', async () => {
        const name = 'Association Name'
        const description = 'Association Description'

        promptSpy.mockResolvedValueOnce({ name, description })

        await handleCreateAssociation(mockedTezosToolkit)

        expect(createAssociationSpy).toBeCalledWith(
          { name: 'Association Name', description: 'Association Description' },
          mockedTezosToolkit
        )
      })
    })

    describe('when createAssociation is called with error', () => {
      it('should throw error with correct message', async () => {
        const error = {
          lastError: {
            with: {
              string: 'Custom Error'
            }
          }
        }
        createAssociationSpy.mockRejectedValueOnce(error)

        const name = 'Association Name'
        const description = 'Association Description'

        promptSpy.mockResolvedValueOnce({ name, description })

        await expect(handleCreateAssociation(mockedTezosToolkit)).rejects.toThrow('Custom Error')
      })
    })
  })

  describe('handleJoinAssociation', () => {
    describe('when joinAssociation is called with success', () => {
      it('should join an association with provided name', async () => {
        const associationsByName = ['Association 1', 'Association 2']
        const associationChoose = 'Association 1'

        promptSpy.mockResolvedValueOnce({ choice: associationChoose })
        await handleJoinAssociation(associationsByName, mockedTezosToolkit)

        expect(joinAssociationSpy).toBeCalledWith(associationChoose, mockedTezosToolkit)
      })
    })

    describe('when joinAssociation is called with error', () => {
      it('should throw error with correct message', async () => {
        const error = {
          lastError: {
            with: {
              string: 'Custom Error'
            }
          }
        }
        joinAssociationSpy.mockRejectedValueOnce(error)
        const associationsByName = ['Association 1', 'Association 2']
        const associationChoose = 'Association 1'

        promptSpy.mockResolvedValueOnce({ choice: associationChoose })

        await expect(handleJoinAssociation(associationsByName, mockedTezosToolkit)).rejects.toThrow('Custom Error')
      })
    })
  })

  describe('handleGetAssociations', () => {
    describe('when getAssociations is called with success', () => {
      it('should call getAssociations', async () => {
        getAssociationsSpy.mockResolvedValueOnce([
          {
            name: 'Association 1',
            description: 'Association 1 description'
          },
          {
            name: 'Association 2',
            description: 'Association 2 description'
          }
        ])
        const associations = await handleGetAssociations(mockedTezosToolkit)

        expect(getAssociationsSpy).toBeCalledWith(mockedTezosToolkit)
        expect(associations).toStrictEqual(
          [
            {
              description: 'Association 1 description',
              name: 'Association 1'
            },
            {
              description: 'Association 2 description',
              name: 'Association 2'
            }
          ]
        )
      })
    })

    describe('when getAssociations is called with error', () => {
      it('should throw error with correct message', async () => {
        const error = {
          message: 'Custom Error'
        }
        getAssociationsSpy.mockRejectedValueOnce(error)

        await expect(handleGetAssociations(mockedTezosToolkit)).rejects.toThrow('Custom Error')
      })
    })
  })

  describe('handleGetAssociationDetails', () => {
    describe('when getAssociationDetails is called with success', () => {
      it('should get association details with provided name', async () => {
        const associationsByName = ['Association 1', 'Association 2']
        const associationChoose = 'Association 1'
        getAssociationDetailsSpy.mockResolvedValueOnce(
          {
            name: 'Association 1',
            description: 'Association 1 description'
          }
        )
        promptSpy.mockResolvedValueOnce({ choice: associationChoose })

        const association = await handleGetAssociationDetails(associationsByName, mockedTezosToolkit)

        expect(getAssociationDetailsSpy).toBeCalledWith(associationChoose, mockedTezosToolkit)
        expect(association).toStrictEqual({
          name: 'Association 1',
          description: 'Association 1 description'
        })
      })
    })

    describe('when getAssociationDetails is called with error', () => {
      it('should throw error with correct message', async () => {
        const error = {
          message: 'Custom Error'
        }
        getAssociationDetailsSpy.mockRejectedValueOnce(error)

        const associationsByName = ['Association 1', 'Association 2']
        const associationChoose = 'Association 1'

        promptSpy.mockResolvedValueOnce({ choice: associationChoose })

        await expect(handleGetAssociationDetails(associationsByName, mockedTezosToolkit)).rejects.toThrow('Custom Error')
      })
    })
  })
})
