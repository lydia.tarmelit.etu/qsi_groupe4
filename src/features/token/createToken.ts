import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { handleCreateToken } from '../../handlers/token/tokenHandlers.js'

/**
 * Creates a token.
 * @param {TezosToolkit} tezos - The instance of the Tezos toolkit.
 * @returns {Promise<void>} A promise resolved once the token is created.
 */
async function createToken (tezos: TezosToolkit): Promise<void> {
  await handleCreateToken(tezos).then(() => {
    console.log(chalk.bgGreenBright('\nVotre token a été créé !!'))
  }).catch((error) => {
    console.log(chalk.bgRed(`\n${error.message}`))
  })
}

export { createToken }
