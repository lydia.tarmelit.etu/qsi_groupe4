import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { handleCreateAssociation } from '../../handlers/association/associationHandlers.js'

/**
 * Creates an association.
 * @param {TezosToolkit} tezos - The instance of the Tezos toolkit.
 * @returns {Promise<void>} A promise resolved once the association is created.
 */
async function createAssociation (tezos: TezosToolkit): Promise<void> {
  await handleCreateAssociation(tezos).then(() => {
    console.log(chalk.bgGreenBright('\nVotre association a été créée !!'))
  }).catch((error) => {
    console.log(chalk.bgRed(`\n${error.message}`))
  })
}

export { createAssociation }
