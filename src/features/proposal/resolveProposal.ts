import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { handleGetSolvableProposals, handleResolveProposal } from '../../handlers/proposal/proposalHandlers.js'

/**
 * Resolves a proposal.
 * @param {TezosToolkit} tezos - The instance of the Tezos toolkit.
 * @returns {Promise<void>} A promise resolved once the proposal is resolved.
 */
async function resolveProposal (tezos: TezosToolkit): Promise<void> {
  await handleGetSolvableProposals(tezos).then(async (response) => {
    const proposalsByTitle: string[] = response.map(proposal => proposal.title)

    if (response.length === 0) throw new Error("Aucune proposition n'est résoluble")

    await handleResolveProposal(proposalsByTitle, tezos).then(() => {
      console.log(chalk.bgGreenBright('\nVous avez résolue la proposition !!'))
    }).catch((error) => {
      console.log(chalk.bgRed(`\n${error.message}`))
    })
  }).catch((error) => {
    console.log(chalk.bgRed(`\n${error.message}`))
  })
}

export { resolveProposal }
