import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { handleGetTokenBalance } from '../../handlers/token/tokenHandlers.js'

/**
 * Return if there's a token.
 * @param {TezosToolkit} tezos - The instance of the Tezos toolkit.
 * @returns {Promise<boolean>} A promise resolved once the token balance is displayed.
 */
async function hasToken (tezos: TezosToolkit): Promise<boolean> {
  let hasToken = false
  await handleGetTokenBalance(tezos).then((response) => {
    hasToken = response > 0
  }).catch((error) => {
    console.log(chalk.bgRed(`\n${error.message}`))
    return false
  })

  return hasToken
}

export { hasToken }
