import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { handleBurnToken } from '../../handlers/token/tokenHandlers.js'

/**
 * Burns token.
 * @param {TezosToolkit} tezos - The instance of the Tezos toolkit.
 * @returns {Promise<void>} A promise resolved once the token is burned.
 */
async function burnToken (tezos: TezosToolkit): Promise<void> {
  await handleBurnToken(tezos).then(() => {
    console.log(chalk.bgGreenBright('\nVous avez brûler un token !!'))
  }).catch((error) => {
    console.log(chalk.bgRed(`\n${error.message}`))
  })
}

export { burnToken }
