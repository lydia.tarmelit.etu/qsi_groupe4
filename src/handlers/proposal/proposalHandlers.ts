// IMPORT EXTERNAL LIBS
import { type TezosToolkit } from '@taquito/taquito'

// IMPORT TYPES
import { type Proposal } from '../../types/Proposal.js'

// IMPORT SERVICES
import { closeProposal, createProposal, getProposals, resolveProposal, voteForProposal } from '../../services/proposal.service.js'
import inquirer from 'inquirer'
import { type Vote } from '../../types/Vote.js'

/**
 * Handles the process of creating a proposal.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<void>} A promise resolved once the proposal creation process is complete.
 */
async function handleCreateProposal (tezos: TezosToolkit): Promise<void> {
  const questions = [
    {
      type: 'input',
      name: 'title',
      message: 'Titre',
      validate: function (input: string) {
        const done = this.async()

        if (input.trim() === '') {
          done('Vous devez remplir ce champ')
        } else {
          done(null, true)
        }
      }
    },
    {
      type: 'input',
      name: 'description',
      message: 'Description',
      validate: function (input: string) {
        const done = this.async()

        if (input.trim() === '') {
          done('Vous devez remplir ce champ')
        } else {
          done(null, true)
        }
      }
    }
  ]

  let proposal: Proposal

  await inquirer.prompt(questions).then(async (answers: { title: string, description: string }) => {
    proposal = {
      title: answers.title,
      description: answers.description
    }
  })

  try {
    await createProposal(proposal, tezos)
  } catch (error) {
    const errorMessage = error.lastError?.with?.string ? error.lastError.with.string : 'Unknown error occurred'
    throw new Error(`${errorMessage}`)
  }
}

/**
 * Handles the process of resolve proposal.
 * @param {string[]} proposals - A list of proposal title.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<void>} A promise resolved once the resolve process is complete.
 */
async function handleResolveProposal (proposals: string[], tezos: TezosToolkit): Promise<void> {
  const questions = [
    {
      type: 'list',
      name: 'choice',
      message: 'Quelle proposition voulez-vous résoudre ?',
      choices: proposals
    }
  ]

  let proposalName: string

  await inquirer.prompt(questions).then(async (answers: { choice: string }) => {
    proposalName = answers.choice
  })

  try {
    await resolveProposal(proposalName, tezos)
  } catch (error) {
    const errorMessage = error.lastError?.with?.string ? error.lastError.with.string : 'Unknown error occurred'
    throw new Error(`${errorMessage}`)
  }
}

/**
 * Handles the process of close proposal.
 * @param {string[]} proposals - A list of proposal title.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<void>} A promise resolved once the close process is complete.
 */
async function handleCloseProposal (proposals: string[], tezos: TezosToolkit): Promise<void> {
  const questions = [
    {
      type: 'list',
      name: 'choice',
      message: 'Quelle proposition voulez-vous clôturer ?',
      choices: proposals
    }
  ]

  let proposalName: string

  await inquirer.prompt(questions).then(async (answers: { choice: string }) => {
    proposalName = answers.choice
  })

  try {
    await closeProposal(proposalName, tezos)
  } catch (error) {
    const errorMessage = error.lastError?.with?.string ? error.lastError.with.string : 'Unknown error occurred'
    throw new Error(`${errorMessage}`)
  }
}

/**
 * Handles the process of listing proposals.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<Proposal[]>} A promise with a list of proposal title.
 */
async function handleGetProposals (tezos: TezosToolkit): Promise<Proposal[]> {
  try {
    return await getProposals(tezos)
  } catch (error) {
    const errorMessage = error.message ? error.message : 'Unknown error occurred'
    throw new Error(`${errorMessage}`)
  }
}

/**
 * Handles the process of listing open proposals.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<Proposal[]>} A promise with a list of open proposal title.
 */
async function handleGetOpenProposals (tezos: TezosToolkit): Promise<Proposal[]> {
  try {
    const proposals: Proposal[] = await getProposals(tezos)
    return proposals.filter(proposal => proposal.isOpen)
  } catch (error) {
    const errorMessage = error.message ? error.message : 'Unknown error occurred'
    throw new Error(`${errorMessage}`)
  }
}

/**
 * Handles the process of listing solvable proposals.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<Proposal[]>} A promise with a list of solvable proposal title.
 */
async function handleGetSolvableProposals (tezos: TezosToolkit): Promise<Proposal[]> {
  try {
    const proposals: Proposal[] = await getProposals(tezos)
    return proposals.filter(proposal => proposal.approveRate >= 51)
  } catch (error) {
    const errorMessage = error.message ? error.message : 'Unknown error occurred'
    throw new Error(`${errorMessage}`)
  }
}

/**
 * Handles the process of vote proposal.
 * @param {string[]} proposals - A list of proposal title.
 * @param {TezosToolkit} tezos - The TezosToolkit instance used for blockchain operations.
 * @returns {Promise<void>} A promise resolved once the close process is complete.
 */
async function handleVoteProposal (proposals: string[], tezos: TezosToolkit): Promise<void> {
  const voteAvailable = ['Yay', 'Nope', 'Pass']

  const propositionQuestions = [
    {
      type: 'list',
      name: 'choice',
      message: 'Pour quelle proposition voulez-vous voter ?',
      choices: proposals
    }
  ]

  let proposalName: string

  await inquirer.prompt(propositionQuestions).then(async (answers: { choice: string }) => {
    proposalName = answers.choice
  })

  const proposalVoteQuestions = [
    {
      type: 'list',
      name: 'choice',
      message: 'Quel est votre vote',
      choices: voteAvailable
    }
  ]

  let vote: Vote

  await inquirer.prompt(proposalVoteQuestions).then(async (answers: { choice: Vote }) => {
    vote = answers.choice
  })

  try {
    await voteForProposal(proposalName, vote, tezos)
  } catch (error) {
    const errorMessage = error.lastError?.with?.string ? error.lastError.with.string : 'Unknown error occurred'
    throw new Error(`${errorMessage}`)
  }
}

export { handleCreateProposal, handleResolveProposal, handleGetProposals, handleCloseProposal, handleGetOpenProposals, handleGetSolvableProposals, handleVoteProposal }
