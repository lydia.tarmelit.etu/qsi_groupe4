import { type Operation, type TezosToolkit } from '@taquito/taquito'
import { type Association } from '../types/Association'

// NEED UPDATE ADDRESS !! (SMART CONTRACT 1: Registre des associations)
const address = 'KT1QTwmF2eptKss2FWbT1rHP5wCERL16kihQ'

async function createAssociation (association: Association, tezos: TezosToolkit): Promise<void> {
  const contract = await tezos.contract.at(address)

  const op: Operation = await contract.methodsObject.registerAssociation(association).send()

  await op.confirmation()
}

// NEED UPDATE ENTRYPOINT !!
async function joinAssociation (associationName: string, tezos: TezosToolkit): Promise<void> {
  // const contract = await tezos.contract.at(address)
  // const op: Operation = await contract.methodsObject.joinAssociation(associationName).send()

  // await op.confirmation()
}

// NEED UPDATE ENTRYPOINT !!
async function getAssociations (tezos: TezosToolkit): Promise<Association[]> {
  const contract = await tezos.contract.at(address)
  const associations: Association[] = []

  const executionContextParams = {
    viewCaller: contract.address
  }

  const associationsMap: Map<string, Association> = await contract.contractViews.listAllAssociations().executeView(executionContextParams)

  associationsMap.forEach((association) => {
    associations.push(association)
  })

  return associations
}

async function getAssociationDetails (associationName: string, tezos: TezosToolkit): Promise<Association> {
  const contract = await tezos.contract.at(address)

  const executionContextParams = {
    viewCaller: contract.address
  }

  const associationDetails: Association = await contract.contractViews.listDetailsAssociations(associationName).executeView(executionContextParams)
  return associationDetails
}

export { createAssociation, joinAssociation, getAssociations, getAssociationDetails }
