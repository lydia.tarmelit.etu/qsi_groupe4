import { type TezosToolkit } from '@taquito/taquito'
import chalk from 'chalk'
import { handleGetBalance } from '../../handlers/balance/balanceHandlers.js'

/**
 * Displays the balance of the wallet.
 * @param {TezosToolkit} tezos - The instance of the Tezos toolkit.
 * @returns {Promise<void>} A promise resolved once the balance is displayed.
 */
async function showBalance (tezos: TezosToolkit): Promise<void> {
  await handleGetBalance(tezos).then((response) => {
    console.log(`\nSolde du portefeuille: ${response} ꜩ`)
  }).catch((error) => {
    console.log(chalk.bgRed(`\n${error.message}`))
  })
}

export { showBalance }
