import figlet from 'figlet'

async function getAsciiArtText (): Promise<void> {
  await new Promise<void>((resolve, reject) => {
    figlet.text(
      'My Asso',
      {
        font: 'Big Money-nw',
        horizontalLayout: 'default',
        verticalLayout: 'default',
        width: 80,
        whitespaceBreak: true
      },
      (err, data) => {
        if (err) {
          console.error('Something went wrong...')
          console.dir(err)
          reject(err)
        } else {
          console.log(data)
          resolve()
        }
      }
    )
  })
}

export { getAsciiArtText }
